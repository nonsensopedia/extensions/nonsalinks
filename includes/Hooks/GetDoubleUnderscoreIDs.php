<?php

namespace NonsaLinks\Hooks;

use MediaWiki\Hook\GetDoubleUnderscoreIDsHook;

class GetDoubleUnderscoreIDs implements GetDoubleUnderscoreIDsHook {

	public function __construct() {
	}

	public static function run( &...$params ) : bool {
		$handler = new static();
		return $handler->onGetDoubleUnderscoreIDs( ...$params );
	}

	public function onGetDoubleUnderscoreIDs( &$doubleUnderscoreIDs ) : bool {
		$doubleUnderscoreIDs = array_merge( $doubleUnderscoreIDs, [
			'ignoreseealso'
		] );
		return true;
	}
}