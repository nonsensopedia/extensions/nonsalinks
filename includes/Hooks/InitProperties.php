<?php

namespace NonsaLinks\Hooks;

use NonsaLinks\SMW\Properties;
use SMW\PropertyRegistry;

class InitProperties {

	public function __construct() {
	}

	public static function run( ...$params ) : bool {
		$handler = new static();
		return $handler->onInitProperties( ...$params );
	}

	public function onInitProperties( PropertyRegistry $propertyRegistry ) : bool {
		$definitions = Properties::getPropertyDefinitions();

		foreach ( $definitions as $propertyId => $definition ) {
			$propertyRegistry->registerProperty(
				$propertyId,
				$definition['type'],
				$definition['label'],
				$definition['viewable'],
				$definition['annotable']
			);

			$propertyRegistry->registerPropertyAlias(
				$propertyId,
				wfMessage( $definition['alias'] )->text()
			);

			$propertyRegistry->registerPropertyAliasByMsgKey(
				$propertyId,
				$definition['alias']
			);

			$propertyRegistry->registerPropertyDescriptionByMsgKey(
				$propertyId,
				$definition['description']
			);
		}

		return true;
	}
}