<?php

namespace NonsaLinks\SMW;

use SMW\SemanticData;

abstract class PropertyAnnotator {
	public abstract function addAnnotation( SemanticData $semanticData );
}